#!G:\Lachus\JDKs\PHP\php-8.0.3\php.exe
<?php
echo "File upload. \n";

if ($args = getopt("u:")) {
    $url = 'http://localhost:80/upload.php';

    $file_field_name = 'fileToUpload';
    $filePath = $args["u"];


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if (function_exists('curl_file_create')) {
        $filePath = curl_file_create($filePath);
    } else {
        $filePath = '@' . realpath($filePath);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
    }

    $postFields = array(
        $file_field_name => $filePath,
        "html" => false
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

    echo $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo curl_error($ch);
    }
    curl_close($ch);
} else {
    echo "You have to choose file to upload (-u).\n";
}