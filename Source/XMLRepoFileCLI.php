<?php
require_once("XMLRepoFile.php");
require_once("Printer.php");

class XMLRepoFileCLI extends XMLRepoFile implements Printer
{
    public function printAll()
    {
        echo "\n";
        self::printColumnVal($this->id);
        self::printColumnVal($this->file_name);
        self::printColumnVal($this->file_path);
    }

    public function printWithoutId()
    {
        echo "\n";
        self::printColumnVal($this->file_name);
        self::printColumnVal($this->file_path);
    }

    public static function printColumnVal($val)
    {
        echo "  $val  |";
    }
}