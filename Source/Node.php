<?php

class Node
{
    protected string $node_id;
    protected string $node_name;
//    private $xml_file_id;

    /**
     * Node constructor.
     * @param $node_id
     * @param $node_name
     */
    public function __construct($node_id, $node_name)
    {
        $this->node_id = $node_id;
        $this->node_name = $node_name;
    }

    /**
     * @return mixed
     */
    public function getNodeId(): string
    {
        return $this->node_id;
    }

    /**
     * @param mixed $node_id
     */
    public function setNodeId(string $node_id): void
    {
        $this->node_id = $node_id;
    }

    /**
     * @return mixed
     */
    public function getNodeName(): string
    {
        return $this->node_name;
    }

    /**
     * @param mixed $node_name
     */
    public function setNodeName(string $node_name): void
    {
        $this->node_name = $node_name;
    }
}