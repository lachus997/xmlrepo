<?php
require_once("XMLRepoFileHTML.php");
require_once("XMLRepoFileCLI.php");
require_once("Node.php");

class ConnectionClass
{
    private string $servername = "localhost";
    private string $username = "xmler";
    private string $password = "password";
    private string $db = "XMLRepo";

    private mysqli|null $conn = null;

    public function __construct()
    {
        $this->connect();
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        echo "Connected successfully \n";
    }

    function connect()
    {
        $this->conn = new mysqli($this->servername, $this->username, $this->password);
    }

    public function disconnect()
    {
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $this->conn->close();
    }

    public function insert_file($file_name, $file_path, $hash)
    {
        $sql = sprintf("INSERT INTO $this->db .Xml_files(file_name, file_path, hash_code) values ('%s','%s','%s')", $this->conn->real_escape_string($file_name), $this->conn->real_escape_string($file_path), $this->conn->real_escape_string($hash));
        if ($this->conn->query($sql)) {
            echo "Files added.";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    public function insert_nodes($file_name, $file_path, $nodes)
    {
        $id_file = $this->get_file_id($file_name, $file_path);
        $sql = "INSERT INTO $this->db .Nodes(node_id, node_name, xml_file_id) values";
        $count = 0;
        foreach ($nodes as $value) {
            if ($count != 0) {
                $sql = $sql . ',';
            }
            $count++;
            $id = $value->getNodeId();
            $name = $value->getNodeName();
            $sql = sprintf($sql . "('%s', '%s' , %s)", $this->conn->real_escape_string($id), $this->conn->real_escape_string($name), $this->conn->real_escape_string($id_file));
        }

        if ($this->conn->query($sql)) {
            echo "Nodes added.";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    private function get_file_id($file_name, $file_path)
    {
        $sql = sprintf("SELECT id from $this->db .Xml_files where (file_name = '%s'and file_path = '%s')", $this->conn->real_escape_string($file_name), $this->conn->real_escape_string($file_path));
        if ($this->conn->query($sql)) {
            return $this->conn->query($sql)->fetch_assoc()['id'];
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
            return null;
        }
    }

    public function search_file($id, $name, $class): ?array
    {
        $sql = "SELECT id, file_name, file_path FROM xmlrepo.xml_files where id = (select xml_file_id from xmlrepo.nodes where";
        if ($id != "") {
            $sql = sprintf($sql . " node_id = '%s'", $this->conn->real_escape_string($id));
        }
        if ($id != "" and $name != "") {
            $sql = $sql . "and ";
        }
        if ($name != "") {
            $sql = sprintf($sql . " node_name = '%s'", $this->conn->real_escape_string($name));
        }
        $sql = $sql . ")";
        if ($res = $this->conn->query($sql)) {
            $objs = [];
            while ($obj = $res->fetch_object($class)) {
                array_push($objs, $obj);
            }
            return $objs;
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
            return null;
        }
    }

    public function select_all(): ?array
    {
        $sql = "SELECT id, file_name, file_path FROM xmlrepo.xml_files ";
        if ($res = $this->conn->query($sql)) {
            $objs = [];
            while ($obj = $res->fetch_object("XMLRepoFile")) {
                array_push($objs, $obj);
            }
            return $objs;
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
            return null;
        }
    }

    public function check_if_stored(string $hash)
    {
        $sql = "SELECT $this->db.xml_is_stored('$hash') as is_stored;";
        if ($res = $this->conn->query($sql)) {
            return $res->fetch_object()->is_stored;
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
            return null;
        }
    }
}