<?php

class XMLRepoFile
{
    protected string $id;
    protected string $file_name;
    protected string $file_path;

//    public function __construct(int $id, string $file_name, string $file_path)
//    {
//        $this->id = $id;
//        $this->file_name = $file_name;
//        $this->file_path = $file_path;
//    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getFileName(): string
    {
        return $this->file_name;
    }

    public function setFileName(string $file_name): void
    {
        $this->file_name = $file_name;
    }

    public function getFilePath(): string
    {
        return $this->file_path;
    }

    public function setFilePath(string $file_path): void
    {
        $this->file_path = $file_path;
    }

    public function printAll()
    {
        echo "<tr>";
        self::printColumnVal($this->id);
        self::printColumnVal($this->file_name);
        self::printColumnVal($this->file_path);
        echo "</tr>";
    }

    public function printWithoutId()
    {
        echo "<tr>";
        self::printColumnVal($this->file_name);
        self::printColumnVal($this->file_path);
        echo "</tr>";
    }

    public static function printColumnVal($val)
    {
        echo "<td>$val</td>";
    }
}