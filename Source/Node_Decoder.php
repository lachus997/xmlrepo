<?php
require_once("Node.php");

class Node_Decoder
{

    public static function domToArray(DOMDocument $in): array
    {
        $nodes = [];
        foreach ($in->getElementsByTagName("*") as $node) {
            if ($node->getAttribute("id") != "") {
                array_push($nodes, new Node(
                    $node->getAttribute("id"),
                    $node->tagName));
            }
        }
        return $nodes;
    }
}